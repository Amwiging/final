﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinBoxScript : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public GameObject player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public AudioSource exitAudio;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource caughtAudio;
    public CanvasGroup deathBackgroundImageCanvasGroup;
    public AudioSource deathAudio;
    public AudioSource musicAudio;


    bool m_IsPlayerAtExit;
    bool m_IsPlayerCaught;
    bool m_IsPlayerDead;
    float m_Timer;
    bool m_HasAudioPlayed;

    void Start()
    {
        musicAudio = GetComponent<AudioSource>();
    }


    void OnTriggerEnter(Collider other)
    {
        //to trigger an event when player hits the trigger
        if (other.gameObject == player)
        {
            //to set isplayeratexit visible when colliding
            PlayerController pMovement = player.GetComponent<PlayerController>();

                m_IsPlayerAtExit = true;
                GameObject.Find("Player").SendMessage("finish");

        }
    }

    public void DeadPlayer()
    {
        //to indicate if the player is caught to call the caught player 
        m_IsPlayerDead = true;
    }

    public void CaughtPlayer()
    {
        //to indicate if the player is caught to call the caught player 
        m_IsPlayerCaught = true;
    }

    void Update()
    {
        //call the win screen and audio when at exit
        if (m_IsPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio);
        }

        //call the caught screen adn audio when caught
        else if (m_IsPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }

        else if (m_IsPlayerDead)
        {
            EndLevel(deathBackgroundImageCanvasGroup, true, deathAudio);
        }



    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }

        m_Timer += Time.deltaTime;
        imageCanvasGroup.alpha = m_Timer / fadeDuration;

        if (m_Timer > fadeDuration + displayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
            }
            else
            {
                Application.Quit();
            }
        }
    }
}

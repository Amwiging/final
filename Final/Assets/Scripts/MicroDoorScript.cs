﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicroDoorScript : MonoBehaviour
{
    public GameObject Door;
    public bool doorisopening;
    public GameObject player;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (doorisopening == true)
        {
            Door.transform.rotation = Quaternion.RotateTowards(Door.transform.rotation, Quaternion.Euler(0.0f, 90.0f, 0.0f), 30f * Time.deltaTime);

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
            doorisopening = true;

    }
}
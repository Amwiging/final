﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChipScript : MonoBehaviour
{
    public GameObject Door;
    public bool doorisopening;
    public GameObject player;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (doorisopening == true)
        {
            Door.transform.rotation = Quaternion.RotateTowards(Door.transform.rotation, Quaternion.Euler(-90.0f, 0.0f, 0.0f), 50f * Time.deltaTime);

        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
            doorisopening = true;

    }
}

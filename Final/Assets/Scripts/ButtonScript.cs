﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ButtonScript : MonoBehaviour
{
    public GameObject Door;
    public bool doorisopening;
    public GameObject player;
    public TextMeshProUGUI startText;


    // Start is called before the first frame update
    void Start()
    {
        startText.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (doorisopening == true)
        {
            Door.transform.Translate(Vector3.up * Time.deltaTime * 10);
        }
        if (Door.transform.position.y > 100f)
        {
            doorisopening = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == player)
            doorisopening = true;
            startText.enabled = false;

    }
}
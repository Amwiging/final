﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using TMPro;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    
    public float speed = 0;
    
    public TextMeshProUGUI timerText;
    public AudioSource audioSource;
    public WinBoxScript winBoxScript;

    private Rigidbody rb;
    private int count;
    private float movementX;
    private float movementY;

    public float jumpPower;
    bool onGround = false;

    public float startTime;
    private bool finished = false;


    // Start is called before the first frame update
    void Start()
    {
        // Assign the Rigidbody to our private rb variable
        rb = GetComponent<Rigidbody>();

        // Set the score to zero
        count = 0;

        // Set the text property of the Win Text UI empty
        

        startTime = Time.time;

    }

    void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, 2f);

        if (finished)
            return;

        float t = Time.time - startTime;

        string minutes = ((int) t / 60).ToString();
        string seconds = (t % 60).ToString("f2");

        timerText.text = minutes + ":" + seconds;

        if (t > 240)
        {
            winBoxScript.CaughtPlayer();
        }

    }

    //Assign the X and Y to allow the player to move
    void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    }

    

    void FixedUpdate()
    {
        // Create a Vector3 variable, and assign X and Z to feature the horizontal and vertical float variables above
        Vector3 movement = new Vector3(movementX, 0.0f, movementY);

        rb.AddForce(movement * speed);

    }

    void OnJump()
    {
        Jump();
    }

    void Jump()
    {
        if (onGround)
            rb.AddForce(Vector3.up * jumpPower);
    }

    private void OnTriggerEnter(Collider other)
    {
        // ..and if the GameObject you intersect has the tag 'Pick Up' assigned to it..
        if (other.gameObject.CompareTag("PickUp"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;

          

        }
    }

    void OnCollisionEnter(Collision other)
    {

        //This alternate version will only crack the egg from falling down (negative on y-axis):
        Vector3 VellwithoutUp = other.relativeVelocity;
        if (VellwithoutUp.y < 0f)
            VellwithoutUp = new Vector3(VellwithoutUp.x, 0f, VellwithoutUp.z);

        if (VellwithoutUp.magnitude > 20f)
        {
            winBoxScript.DeadPlayer();

        }

    }

    void finish()
    {
        finished = true;
        timerText.color = Color.yellow;
    }


}
